var http = require('http');

http.createServer(function(request, response) {

    // 发送 HTTP 头部 
    // HTTP 状态值: 200 : OK
    // 内容类型: text[表情]ain
    response.writeHead(200, { 'Content-Type': 'text/plain' });

    // 发送响应数据 "Hello World"
    let list = {
        list: [
            { name: "11111", price: "价钱", phone: "照片" },
            { name: "11111", price: "价钱", phone: "照片" },
            { name: "11111", price: "价钱", phone: "照片" }
        ]
    }
    response.end(JSON.stringify(list));
}).listen(8888);

// 终端打印如下信息
console.log('Server running at http://127.0.0.1:8888');